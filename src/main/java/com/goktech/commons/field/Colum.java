package com.goktech.commons.field;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.PARAMETER,ElementType.FIELD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Colum {

	String message() default "";//提示信息
	
	int max() default Integer.MAX_VALUE;//最大值用于数字
	
	int min() default Integer.MIN_VALUE;
	
	boolean NotEmpty() default true;//不能为空和null，用于字符窜 默认不可以为空
	
	boolean NotNull() default true;//不能为null 默认不可以为null 
	
	int maxLength() default Integer.MAX_VALUE;//最大值，用户校验字符串 list map  默认Integer的最大值
	
	int minLength() default 0;//最大值，用户校验字符串 list map
	
	String[] regex() default {};//正则表达式
	
	long maxLong() default Long.MAX_VALUE;//最大长整型
	
	long minLong() default Long.MIN_VALUE;//最小长整型
	
	double maxDouble() default Double.MAX_VALUE;
	
	double minDouble() default Double.MIN_VALUE;
	
	short maxShort() default Short.MAX_VALUE;
	
	short minShort() default Short.MIN_VALUE;
	
	String defaultValue() default "";//数据库 默认值
	
	boolean isKey() default false;//数据库是否是主键
	
	String dataBaseType() default "";//数据库字段类型
	
	String comment() default "";//注释
	
	String fieldName() default "";//数据库字段名字
}
