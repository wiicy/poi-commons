/**
 * 
 */
package com.goktech.commons.excel.utils;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;

import com.goktech.commons.excel.utils.abs.FontMediator;

/**
 * @author zhongmh 设置样式
 */
public interface StyleMediatorInterface<T,M>  extends Mediator<M>{

	/**
	 * 底边框样式
	 * 
	 * @param borderBottom
	 * @return
	 */
	public T borderBottom(BorderStyle borderStyle);

	/**
	 * 自动创建新的style对象
	 * 
	 * @return
	 */
	public T style();

	/**
	 * 将传入的对象作为style对象
	 * 
	 * @param cellStyle
	 * @return
	 */
	public T style(CellStyle cellStyle);
	
	/**
	 * 设置底边框颜色;
	 * 
	 * @param indexedColors
	 * @return
	 */
	public T bottomBorderColor(IndexedColors indexedColors);

	/**
	 * 设置左边框;
	 * 
	 * @param borderLeft
	 * @return
	 */
	public T borderLeft(BorderStyle borderStyle);
	/**
	 *  设置左边框颜色;
	 * @param indexedColors
	 * @return
	 */
	public T leftBorderColor(IndexedColors indexedColors);
	/**
	 *  设置右边框;
	 * @param borderRight
	 * @return
	 */
	public T borderRight(BorderStyle borderStyle);
	/**
	 * 设置右边框颜色;
	 * @param indexedColors
	 * @return
	 */
	public T rightBorderColor(IndexedColors indexedColors);
	/**
	 *  设置顶边框
	 * @param borderTop
	 * @return
	 */
	public T borderTop(BorderStyle borderStyle);
	/**
	 * 设置顶边框颜色;
	 * @param colors
	 * @return
	 */
	public T topBorderColor(IndexedColors colors);
	/**
	 * 设置是否自动换行
	 * @param wrapText
	 * @return
	 */
	public T wrapText(boolean wrapText);
	/**
	 * 设置水平对齐的样式为居中对齐;
	 * @param alignment
	 * @return
	 */
	public T alignment(HorizontalAlignment align);
	/**
	 *  设置垂直对齐的样式为居中对齐;
	 * @param alignment
	 * @return
	 */
	public T verticalAlignment(VerticalAlignment align);
	/**
	 * 设置背景颜色
	 * @param indexedColors
	 * @return
	 */
	public T fillBackgroundColor(IndexedColors indexedColors);
	/**
	 * 
	 * @param indexedColors
	 * @return
	 */
	public T fillForegroundColor(IndexedColors indexedColors);
	/**
	 * 
	 * @param fillPattern
	 * @return
	 */
	public T fillPattern(FillPatternType fp);

	public FontMediator<T> font();
}
