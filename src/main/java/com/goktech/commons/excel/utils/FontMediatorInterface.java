/**
 * 
 */
package com.goktech.commons.excel.utils;

/**
 * @author zhongmh
 *
 */
public interface FontMediatorInterface<T,M> extends Mediator<M>{
	
	public T fontName(String fontName);
	
	public T italic(boolean italic);
	
	public T height(short fontHeight);
	
	public T heightInPoints(short heightInPoints);
}
