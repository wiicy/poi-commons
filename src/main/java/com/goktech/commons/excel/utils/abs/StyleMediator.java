/**
 * 
 */
package com.goktech.commons.excel.utils.abs;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

import com.goktech.commons.excel.ExcelStyle;
import com.goktech.commons.excel.ExcelStyleResolver;
import com.goktech.commons.excel.Style;
import com.goktech.commons.excel.utils.StyleMediatorInterface;

/**
 * @author zhongmh
 *
 */
@Style
public class StyleMediator<M>  implements StyleMediatorInterface<StyleMediator<M>,M>{

	private Workbook workbook;
	private Row row;
	private Cell cell;
	private List<CellStyle> styleList;
	private M m;
	
	ExcelStyle esr;
	
	public StyleMediator(M m,Workbook workbook,Row row) {
		this.workbook = workbook;
		this.row = row;
		this.m = m;
		this.styleList = new ArrayList<>();
		esr = new ExcelStyleResolver(StyleMediator.class,workbook);
	}
	
	public StyleMediator(M m,Workbook workbook,Row row,ExcelStyle esr) {
		this.workbook = workbook;
		this.row = row;
		this.m = m;
		this.styleList = new ArrayList<>();
		if(esr == null){
			esr = new ExcelStyleResolver(StyleMediator.class,workbook);
		}
		this.esr = esr;
	}
	
	public StyleMediator(M m,Workbook workbook,Cell cell) {
		this.workbook = workbook;
		this.cell = cell;
		this.m = m;
		this.styleList = new ArrayList<>();
		esr = new ExcelStyleResolver(StyleMediator.class,workbook);
	}
	
	public StyleMediator(M m,Workbook workbook,Cell cell,ExcelStyle esr) {
		this.workbook = workbook;
		this.cell = cell;
		this.m = m;
		this.styleList = new ArrayList<>();
		if(esr == null){
			esr = new ExcelStyleResolver(StyleMediator.class,workbook);
		}
		this.esr = esr;
	}

	@Override
	public StyleMediator<M> borderBottom(BorderStyle borderStyle) {
		this.cellStyle().setBorderBottom(borderStyle);
		return this;
	}

	@Override
	public StyleMediator<M> style() {
		this.styleList.add(esr.getDefaultStyle());
		return this;
	}

	@Override
	public StyleMediator <M>style(CellStyle cellStyle) {
		this.styleList.add(cellStyle);
		return this;
	}

	@Override
	public StyleMediator<M> bottomBorderColor(IndexedColors indexedColors) {
		this.cellStyle().setBottomBorderColor(indexedColors.getIndex());
		return this;
	}

	@Override
	public StyleMediator<M> borderLeft(BorderStyle borderStyle) {
		this.cellStyle().setBorderLeft(borderStyle);
		return this;
	}

	@Override
	public StyleMediator <M>leftBorderColor(IndexedColors indexedColors) {
		this.cellStyle().setLeftBorderColor(indexedColors.getIndex());
		return this;
	}

	@Override
	public StyleMediator<M> borderRight(BorderStyle borderStyle) {
		this.cellStyle().setBorderRight(borderStyle);
		return this;
	}

	@Override
	public StyleMediator<M> rightBorderColor(IndexedColors indexedColors) {
		this.cellStyle().setRightBorderColor(indexedColors.getIndex());
		return this;
	}

	@Override
	public StyleMediator<M> borderTop(BorderStyle borderStyle) {
		this.cellStyle().setBorderTop(borderStyle);
		return this;
	}

	@Override
	public StyleMediator<M> topBorderColor(IndexedColors colors) {
		this.cellStyle().setTopBorderColor(colors.getIndex());
		return this;
	}

	@Override
	public StyleMediator<M> wrapText(boolean wrapText) {
		this.cellStyle().setWrapText(wrapText);
		return this;
	}

	@Override
	public StyleMediator<M> alignment(HorizontalAlignment align) {
		this.cellStyle().setAlignment(align);
		return this;
	}

	@Override
	public StyleMediator<M> verticalAlignment(VerticalAlignment align) {
		this.cellStyle().setVerticalAlignment(align);
		return this;
	}

	@Override
	public StyleMediator<M> fillBackgroundColor(IndexedColors indexedColors) {
		this.cellStyle().setFillBackgroundColor(indexedColors.getIndex());
		return this;
	}

	@Override
	public StyleMediator<M> fillForegroundColor(IndexedColors indexedColors){
		this.cellStyle().setFillForegroundColor(indexedColors.getIndex());
		return this;
	}

	@Override
	public StyleMediator<M> fillPattern(FillPatternType fp) {
		this.cellStyle().setFillPattern(fp);
		return this;
	}

	@Override
	public M conver() {
		if(this.row != null ) {
			this.row.setRowStyle(this.cellStyle());
		}
		if(this.cell != null) {
			this.cell.setCellStyle(this.cellStyle());
		}
		return m;
	}

	@Override
	public FontMediator<StyleMediator<M>> font() {
		FontMediator<StyleMediator<M>> fontMediator = new FontMediator<StyleMediator<M>>(cellStyle(), workbook, this);
		return fontMediator;
	}
	private CellStyle cellStyle(){
		return this.styleList.get(this.styleList.size() - 1);
	}
}
