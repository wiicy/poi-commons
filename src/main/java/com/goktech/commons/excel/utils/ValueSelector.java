package com.goktech.commons.excel.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;

import com.goktech.commons.excel.Excel;
import com.goktech.commons.utils.StringUtils;

/**
 * @author 24252
 *
 */
public class ValueSelector {

	private final static String STRING = "java.lang.String";
	
	private final static String DATE = "java.util.Date";
	
	private final static String BIGDECIMAL = "java.math.BigDecimal";
	
	private final static String LONG = "java.lang.Long";
	
	private final static String INTEGER = "java.lang.Integer";
	
	private final static String DOUBLE = "java.lang.Double";
	
	private final static String NULL = "NULL";
	
	
	public static Object getValue(Cell cell, Field field){
		int cellType = cell.getCellType();
		if(cellType == Cell.CELL_TYPE_BLANK || cellType == Cell.CELL_TYPE_ERROR){
			return null;
		}
		if(cellType == Cell.CELL_TYPE_STRING){
			return cell.getStringCellValue();
		}
		if(cellType == Cell.CELL_TYPE_BOOLEAN){
			return cell.getBooleanCellValue();
		}
		if(cellType == Cell.CELL_TYPE_NUMERIC){
			return cell.getNumericCellValue();
		}
		return cell.getStringCellValue();
	}
	



	public static Object getValue(String rowString,Field field){
		if(field == null){
			throw new NullPointerException("rowString-> {"+rowString+"} ;field 为空");
		}
		String fieldType = field.getType().getName();
		if(rowString == null || rowString.equals(NULL) || StringUtils.isEmpty(rowString)){
			return null;
		}
		if(STRING.equals(fieldType)){
			
			return rowString;
		}
		if(INTEGER.equals(fieldType)){
			
			return Integer.parseInt(rowString);
		}
		if(BIGDECIMAL.equals(fieldType)){
			return new BigDecimal(rowString);
		}
		if(LONG.equals(fieldType)){
			
			return Long.parseLong(rowString);
		}
		if(DATE.equals(fieldType)){
			
			return rowString;
		}
		if(DOUBLE.equals(fieldType)){
			
			return Double.parseDouble(rowString);
		}
		return rowString;
	}
	
	public static void getValue(Object value,Cell cell,Excel excel){
		if(value == null)
			value = excel.defaultValue();
		if (value instanceof String) {
			cell.setCellValue((String)value);
		} else if (value instanceof Double || value instanceof Float) {
			cell.setCellValue((Double) (value));
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof BigDecimal) {
			cell.setCellValue(((BigDecimal) value).doubleValue());
		}
	}
	
	public static void getValue(Object value,Cell cell,com.goktech.commons.excel.Cell dataCell){
		if(value == null)
			value = dataCell.value();
		if (value instanceof String) {
			cell.setCellValue((String)value);
		} else if (value instanceof Double || value instanceof Float) {
			cell.setCellValue((Double) (value));
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof BigDecimal) {
			cell.setCellValue(((BigDecimal) value).doubleValue());
		}
	}
	
	public static void setValue(Object value,Cell cell){
		if (value instanceof String) {
			cell.setCellValue((String)value);
		} else if (value instanceof Double || value instanceof Float) {
			cell.setCellValue((Double) (value));
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof BigDecimal) {
			cell.setCellValue(((BigDecimal) value).doubleValue());
		}
	}
}
