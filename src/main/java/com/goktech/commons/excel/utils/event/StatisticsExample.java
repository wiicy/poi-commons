/**
 * 
 */
package com.goktech.commons.excel.utils.event;

import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.Cell;

/**
 * @author zhongmh
 * @email yunmaozj@163.com
 * 2017年12月17日下午6:03:21
 */
public class StatisticsExample implements StatsticsEvent{

	public BigDecimal result = new BigDecimal("0.1");
	
	@Override
	public void listener(Cell cell,String fieldName) {
		if("inArea".equals(fieldName)){
			result = this.result.add(new BigDecimal(cell.getNumericCellValue()));
		}
	}
	
	public BigDecimal getResult(){
		return result;
	}

}
