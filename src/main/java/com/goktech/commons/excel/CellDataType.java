/**
 * 
 */
package com.goktech.commons.excel;

/**
 * @author 24252
 *
 */
public enum CellDataType {  
    BOOL, ERROR, FORMULA, INLINESTR, SSTINDEX, NUMBER, DATE, NULL  
} 
