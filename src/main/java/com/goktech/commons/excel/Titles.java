/**
 * 
 */
package com.goktech.commons.excel;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhongmh
 * @email yunmaozj@163.com
 * @createTime 2018年1月3日下午4:43:57
 * @desc 
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Titles {

}
