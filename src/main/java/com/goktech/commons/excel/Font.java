package com.goktech.commons.excel;

import org.apache.poi.hssf.usermodel.HSSFFont;

/**
 * excel导出 字体样式
 * 
 * @author echoyu on 2017年10月27日
 *
 */
public @interface Font {

	/**
	 * 字号
	 * @return
	 */
	short fontHeightInPoints() default 11;
	
	String fontName() default HSSFFont.FONT_ARIAL;
	/**
	 * 设置字体的高度
	 */
	short fontHeight() default 20;
	
	/**
	 * 是否使用斜体
	 * @return
	 */
	boolean italic() default false;
	
	
}
