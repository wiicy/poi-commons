package com.goktech.commons.excel;

/**
 * excel导出设置格式
 * 
 * @author echoyu on 2017年10月27日
 *
 */
public class ExcelException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8476176152037627487L;

	static{
		//避免类加载器死锁
		ExcelException.class.getName();
	}

	public ExcelException() {
		super();
	}

	public ExcelException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ExcelException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExcelException(String message) {
		super(message);
	}

	public ExcelException(Throwable cause) {
		super(cause);
	}
	
	
}
