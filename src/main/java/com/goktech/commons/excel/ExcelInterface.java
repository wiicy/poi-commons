/**
 * 
 */
package com.goktech.commons.excel;

import java.io.IOException;
import java.io.OutputStream;

/**
 * excel导出样式
 * 
 * @author echoyu on 2017年10月27日
 *
 */
public interface ExcelInterface {

	void write(OutputStream outputStream) throws IOException;
	
}
