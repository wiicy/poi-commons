/**
 * 
 */
package com.goktech.commons.excel;

/**
 * @author 24252
 * 统计监听器，主要接收统计后的结果集，然后进行处理
 */
public interface StatisticsListener {

	public Object handle(Object result);
}
