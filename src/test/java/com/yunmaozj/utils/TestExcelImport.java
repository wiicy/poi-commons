/**
 * 
 */
package com.yunmaozj.utils;

import java.io.FileNotFoundException;
import java.util.List;

import com.goktech.commons.excel.utils.reader.ExcelBigDataImport;
import org.junit.Ignore;
import org.junit.Test;

import com.goktech.commons.excel.utils.reader.Excel2003Import;
import com.goktech.commons.excel.utils.reader.ExcelImport;
import com.yunmaozj.bean.Entity;
import com.yunmaozj.bean.Government;

/**
 * @author Administrator
 *
 */
public class TestExcelImport {

	private final String path = "政府官网信息表.xlsx";
	
	@Test
	public void testImport() throws FileNotFoundException {
		List<Government> list = (List<Government>) new ExcelImport(Government.class, path).run(1).getList();
		System.out.println(list.size());
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i).toString());

			try {
				if(i > 3200)
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		new TestExcelImport().testImport();
	}
	
	@Ignore
	@Test
	public void testImport2() throws FileNotFoundException {
		List<Entity> list = (List<Entity>) new ExcelImport(Entity.class, "E:\\DownloadFile\\新建 Microsoft Excel 工作表.xlsx").run().getList();
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i).toString());
		}
	}
	
	private String path2003 = "E:\\DownloadFile\\积极分子.xls";
	
	@Test
	public void testImport3() throws FileNotFoundException {
		List<Entity> list = (List<Entity>) new Excel2003Import(Entity.class, path2003).run().getList();
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i).toString());
		}
	}
}
