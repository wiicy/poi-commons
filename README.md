## excel导出工具类的二次封装
[项目原地址](http://git.cqwzxt.com/summary/~study%2Fgoktech-commons.git)
## charge 4.0 版本| [charge版本跳转](charge4.0版本说明.md)
这个版本对导出做了很多完善。样式复杂化，导入也支持大数据导入。
## tranx 1.0 版本| [tanx版本跳转](tranx1.0.md)
这个版本，对导出和导入都进行了大幅度的改动,因为工作需要，发现原来导入导出有点复杂，所以决定优化。这个版本将会增加无模板类的导入导出，以及支持jdbc导出



##  maven地址
```
	<repositories>
		<repository>
			<id>releases</id>
			<name>Nexus Repository</name>
			<url>http://nexus.cqwzxt.com/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>
```

```
	<dependency>
		<groupId>cn.goktech</groupId>
		<artifactId>goktech-commons</artifactId>
		<version>1.6.0</version>
	</dependency>
```

网页跳转[链接](http://nexus.cqwzxt.com/nexus/content/repositories/releases/cn/goktech/goktech-commons/)
<br/><br/>
[快速使用指南](快速使用指南.md)


[作者](http://www.yunmaozj.com)联系方式：
1. 邮箱:yunmaozj@163.com
2. qq:2425232535
3. 问题反馈群及意见群:[564053346](https://jq.qq.com/?_wv=1027&k=5u8ipX0)

